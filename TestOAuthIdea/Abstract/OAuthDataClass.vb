﻿Imports System.Collections.Specialized
Imports TestOAuthIdea

MustInherit Class OAuthDataClass
    Implements IOAuthDataClass

    Public MustOverride Property Implementation As IOAuthDataStore(Of IOAuthDataClass, Integer) Implements IOAuthDataClass.Implementation

    Public Function GetDataByScope(scope As OAuthScope) As ICollection(Of String) Implements IOAuthDataClass.GetDataByScope
        Dim result As New NameValueCollection
        For i = 0 To scope.FieldName.Count - 1

            Dim propInfo As System.Reflection.PropertyInfo = Me.GetType().GetProperty(scope.FieldName(i))
            result.Add(scope.FieldList(i), propInfo.GetValue(Me).ToString)
        Next
        Return result
    End Function
End Class