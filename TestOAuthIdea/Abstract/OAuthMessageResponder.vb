﻿MustInherit Class OAuthMessageResponder
    Implements IOAuthMessageResponder

    Public Property OAuthData As IOAuthDataClass Implements IOAuthMessageResponder.OAuthData
    Property SupportedScope As ICollection(Of IOAuthScope) Implements IOAuthMessageResponder.SupportedScope

    Sub New(supportedScope As IOAuthScope)
        _SupportedScope = supportedScope
    End Sub
    Public Function IsScopeValid(checkingScope As IOAuthScope) As Boolean Implements IOAuthMessageResponder.IsScopeValid
        If SupportedScope.Contains(checkingScope) Then
            Return True
        Else
            Return False
        End If
    End Function

    Overridable Function CreateResponse(scope As IOAuthScope) As ResponseMessage Implements IOAuthMessageResponder.CreateResponse
        'Checking access token blah blah blah
        If IsScopeValid(scope) Then
            Return CreateSuccessResponse(scope)
        Else
            Return CreateFailureResponse(New ErrorCode(403))
        End If

    End Function

    MustOverride Function CreateSuccessResponse(OAuthScope As IOAuthScope) As ResponseMessage Implements IOAuthMessageResponder.CreateSuccessResponse
    MustOverride Function CreateFailureResponse(code As ErrorCode) As ResponseMessage Implements IOAuthMessageResponder.CreateFailureResponse

End Class
