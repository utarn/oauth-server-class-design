﻿Imports TestOAuthIdea

MustInherit Class OAuthScope
    Implements IEquatable(Of OAuthScope), IOAuthScope

    Protected _name As String
    Protected _fieldName As ICollection(Of String)
    Protected _fieldList As ICollection(Of String)
    ReadOnly Property Name As String Implements IOAuthScope.Name
        Get
            Return _name
        End Get
    End Property
    ReadOnly Property FieldName As ICollection(Of String) Implements IOAuthScope.FieldName
        Get
            Return _fieldName
        End Get
    End Property
    ReadOnly Property FieldList As ICollection(Of String) Implements IOAuthScope.FieldList
        Get
            Return _fieldList
        End Get
    End Property

    Private _implementation As IOAuthDataStore(Of IOAuthScope, String)
    Public Property Implementation As IOAuthDataStore(Of IOAuthScope, String) Implements IOAuthScope.Implementation
        Get
            Return _implementation
        End Get
        Set(value As IOAuthDataStore(Of IOAuthScope, String))
            _implementation = value
        End Set
    End Property

    Public Overrides Function Equals(obj As Object) As Boolean
        Dim anotherScope = TryCast(obj, OAuthScope)
        If anotherScope IsNot Nothing Then
            Return Equals(anotherScope)
        Else
            Return False
        End If
    End Function
    Public Overloads Function Equals(anotherScope As OAuthScope) As Boolean Implements IEquatable(Of OAuthScope).Equals
        Return IIf(anotherScope.Name = Me.Name, True, False)
    End Function

End Class