﻿Class OpenIDScope
    Inherits OAuthScope
    Sub New()
        _name = "openid"
        _fieldName = New List(Of String)(New String() {"Fullname", "Id", "Email", "Status", "Gender"})
        _fieldList = New List(Of String)(New String() {"name", "profile", "email", "email_verified", "gender"})
    End Sub

End Class