﻿Class InMemoryMemberDataStore
    Implements IOAuthDataStore(Of MemberOAuthDataClass, Integer)

    Public AllData As List(Of MemberOAuthDataClass)
    Sub New()
        AllData = New List(Of MemberOAuthDataClass) From {
         New MemberOAuthDataClass() With {.Id = 1, .FullName = "Peter Augusting", .Email = "peter.a@christcollege.edu", .Status = True, .Gender = "male"},
         New MemberOAuthDataClass() With {.Id = 2, .FullName = "Vinay Kumar", .Email = "vinay.k@christcollege.edu", .Status = True, .Gender = "male"},
         New MemberOAuthDataClass() With {.Id = 3, .FullName = "Kajal Barot", .Email = "kajal.b@christcollege.edu", .Status = True, .Gender = "femail"}}

    End Sub

    Public Function Create(data As MemberOAuthDataClass) As Boolean Implements IOAuthDataStore(Of MemberOAuthDataClass, Integer).Create
        Try
            AllData.Add(data)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Delete(data As MemberOAuthDataClass) As Boolean Implements IOAuthDataStore(Of MemberOAuthDataClass, Integer).Delete
        Try
            AllData.Remove(data)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function FindById(id As Integer) As MemberOAuthDataClass Implements IOAuthDataStore(Of MemberOAuthDataClass, Integer).FindById
        Return AllData.FirstOrDefault(Function(m) m.Id = id)
    End Function

    Public Function Update(data As MemberOAuthDataClass) As Boolean Implements IOAuthDataStore(Of MemberOAuthDataClass, Integer).Update
        Try
            Dim toUpdate = AllData.First(Function(m) m.Id = data.Id)
            AllData.Remove(toUpdate)
            AllData.Add(data)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class
