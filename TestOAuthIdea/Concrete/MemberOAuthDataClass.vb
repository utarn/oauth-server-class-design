﻿Imports TestOAuthIdea

Class MemberOAuthDataClass
    Inherits OAuthDataClass

    Public Property FullName As String
    Public Property Id As Integer
    Public Property Email As String
    Public Property Status As Boolean
    Public Property Gender As Boolean

    Private Shared _implementation As IOAuthDataStore(Of MemberOAuthDataClass, Integer)
    Public Overrides Property implementation As IOAuthDataStore(Of IOAuthDataClass, Integer)
        Get
            Return _implementation
        End Get
        Set(value As IOAuthDataStore(Of IOAuthDataClass, Integer))
            _implementation = value
        End Set
    End Property

    Public Shared Function GetMemberById(id As Integer) As MemberOAuthDataClass
        Return _implementation.FindById(id)
    End Function



End Class