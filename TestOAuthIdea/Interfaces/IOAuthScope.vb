﻿Interface IOAuthScope
    Property Implementation As IOAuthDataStore(Of IOAuthScope, String)
    ReadOnly Property Name As String
    ReadOnly Property FieldName As ICollection(Of String)
    ReadOnly Property FieldList As ICollection(Of String)
End Interface