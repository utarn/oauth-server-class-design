﻿Interface IOAuthMessageResponder
    Property OAuthData As IOAuthDataClass
    Property SupportedScope As ICollection(Of IOAuthScope)
    Function IsScopeValid(checkingScope As IOAuthScope) As Boolean
    Function CreateResponse(scope As IOAuthScope) As ResponseMessage
    Function CreateSuccessResponse(OAuthScope As IOAuthScope) As ResponseMessage
    Function CreateFailureResponse(code As ErrorCode) As ResponseMessage
End Interface