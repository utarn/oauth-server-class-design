﻿Interface IOAuthDataClass
    Property Implementation As IOAuthDataStore(Of IOAuthDataClass, Integer)
    Function GetDataByScope(scope As OAuthScope) As ICollection(Of String)
End Interface
