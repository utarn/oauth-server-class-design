﻿Interface IOAuthDataStore(Of TSource, TKey)

    Function Create(data As TSource) As Boolean
    Function Delete(data As TSource) As Boolean
    Function Update(data As TSource) As Boolean
    Function FindById(id As TKey) As TSource

End Interface
